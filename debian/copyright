Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: bzip3
Source: https://github.com/kspalaiologos/bzip3

Files: *
Copyright: 2022-2023, Kamila Szewczyk <kspalaiologos@gmail.com>
License: LGPL-3+

Files: include/libsais.h
Copyright: 2021-2022, Ilya Grebnov <ilya.grebnov@gmail.com>
           2022, Kamila Szewczyk <kspalaiologos@gmail.com>
License: Apache-2.0
 On Debian systems the full text of the Apache-2.0 license can be found in
 /usr/share/common-licenses/Apache-2.0.

Files: build-aux/*
Copyright: 2011, Daniel Richard G <skunk@iSKUNK.ORG>
           2019, Marc Stevens <marc.stevens@cwi.nl>
           2008, Steven G. Johnson <stevenj@alum.mit.edu>
License: GPL-3+ with AutoConf exception

Files: build-aux/ax_check_compile_flag.m4
Copyright: 2008, Guido U. Draheim <guidod@gmx.de>
           2011, Maarten Bosmans <mkbosmans@gmail.com>
License: FSFAP

Files: build-aux/git-version-gen
Copyright: 2007-2012, Free Software Foundation, Inc
License: GPL-3+

Files: bz3grep
Copyright: 2003, Thomas Klausner
License: BSD-2-clause

Files: debian/*
Copyright: 2022-2025 Nobuhiro Iwamatsu <iwamatsu@debian.org>
License: LGPL-3+

License: LGPL-3+
 This program is free software; you can redistribute it and/or modify it under
 the terms of the GNU Lesser General Public License as published by the Free
 Software Foundation; either version 3 of the License, or (at your option) any
 later version.  See http://www.gnu.org/copyleft/lgpl.html for the full text
 of the license.
 .
 On Debian systems, a copy of the LGPL version 3 can be found in
 /usr/share/common-licenses/LGPL-3.

License: GPL-3+ with AutoConf exception
 This file is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 It is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with it.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the full text of the GNU General Public License version 3
 can be found in the file `/usr/share/common-licenses/GPL-3'.
 .
 As a special exception, the respective Autoconf Macro's copyright owner
 gives unlimited permission to copy, distribute and modify the configure
 scripts that are the output of Autoconf when processing the Macro. You
 need not follow the terms of the GNU General Public License when using
 or distributing such scripts, even though portions of the text of the
 Macro appear in them. The GNU General Public License (GPL) does govern
 all other use of the material that constitutes the Autoconf Macro.
 .
 This special exception to the GPL applies to versions of the Autoconf
 Macro released by the Autoconf Archive. When you make and distribute a
 modified version of the Autoconf Macro, you may extend this special
 exception to the GPL to apply to your modified version as well.

License: GPL-3+
 This program is free software; you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation; either version 3 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be useful, but WITHOUT ANY
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along with
 this package; if not, write to the Free Software Foundation, Inc., 51 Franklin
 St, Fifth Floor, Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public License version 3
 can be found in the file `/usr/share/common-licenses/GPL-3'.

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE NETBSD FOUNDATION, INC. AND CONTRIBUTORS
 ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

License: FSFAP
 Copying and distribution of this file, with or without modification, are
 permitted in any medium without royalty provided the copyright notice
 and this notice are preserved. This file is offered as-is, without any
 warranty.
